const abs = require('./s2ab2s');
// const crc = require('node-crc');
const encodeAlarm=({channelid,alarmMessage,token})=>{
    if(typeof channelid === "undefined" || typeof alarmMessage === "undefined" || typeof token === "undefined"){
        throw new TypeError("One or more arguments are undefined")
    }
    if(channelid>255) throw new RangeError("Channel id value limit exceeded. Value ranges from 0 to 255");
    if(token>255) throw new RangeError("Token value limit exceeded. Value ranges from 0 to 255");
    if(alarmMessage.length > 45) throw new RangeError("Message length exceeded. Maximum is 45 characters");
    let buf = new ArrayBuffer(3);
    let alarmProps = new Uint8Array(buf);
    alarmProps[0] = 0x01;
    alarmProps[1] = token;
    alarmProps[2] = channelid;
    let propBuf = Buffer.from(alarmProps,'hex');
    let mAB = abs.stringToArrayBuffer(alarmMessage);
    let msgBuf = Buffer.from(mAB,'hex');
    let fin = Buffer.concat([propBuf, msgBuf]);
    // let cr = crc.crc8(fin).toString('hex');
    // fin = Buffer.concat([fin,Buffer.from(cr,'hex')]);
    return { 
        base64: fin.toString('base64'),
        hex:fin.toString('hex')
      }
}

const decodeAlarm=(msgArray)=>{
    let alarmToken = parseInt(msgArray[0],16);
    let alarmType = parseInt(msgArray[1],16);
    var alarmMessage = msgArray.slice(2).join("");
    alarmMessage = Buffer.from(alarmMessage, 'hex');
    alarmMessage = abs.arraybufferToString(alarmMessage)
    return {
        alarmType: alarmType,
        message:alarmMessage,
        token:alarmToken
    }
}

exports.encodeAlarm = encodeAlarm;
exports.decodeAlarm = decodeAlarm;