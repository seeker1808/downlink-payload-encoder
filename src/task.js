const abs = require('./s2ab2s')
// const crc = require('node-crc');
const encodeTask = ({tasktype, taskpriority, taskdeadline_min=0, taskdeadline_ts=0, tasktoken, 
  isrejectable=false, taskextra=[],taskmessage=" "}) => {
      if(typeof tasktype === "undefined" || typeof taskpriority === "undefined" || typeof tasktoken === "undefined"){
          throw new TypeError("One or more arguments are undefined");
      }
      if(tasktype>63) throw new RangeError("Task type value limit exceeded. Value ranges from 0 to 63");
      if(taskpriority>3) throw new RangeError("Task priority value limit exceeded. Value ranges from 0 to 3");
      if(tasktoken>255) throw new RangeError("Task token value limit exceeded. Value ranges from 0 to 255");
      if( typeof isrejectablea === "boolean") throw new TypeError("Task isrejectable accepts only boolean value");
      if(taskdeadline_min>255) throw new RangeError("Task deadline (mins) value limit exceeded. Value ranges from 0 to 7650");
      if(taskmessage.length > 35) throw new RangeError("Task message length exceeded. Maximum is 35 characters");
      let taskClass = (tasktype<<2) + taskpriority;
      let buf = new ArrayBuffer(5);
      let taskProps = new Uint8Array(buf);
      let finalBuff=[];
      taskProps[0] = 0x11;
      taskProps[1] = tasktoken;
      taskProps[2] = taskClass;
      taskProps[3] = isrejectable? 0xFF: 0x00;
      taskProps[4] = parseInt(taskdeadline_min/30);
      let propBuf = Buffer.from(taskProps,'hex');
      finalBuff.push(propBuf);
      if(taskdeadline_min==0 ){
        var timeStampBuff;
        if(taskdeadline_ts !=0){
            let buf = new ArrayBuffer(4);
            let tsBuf = new Uint8Array(buf);
            let ts = (new Date(taskdeadline_ts).getTime()).toString(16).match(/.{1,2}/g);
            for(let i = 0; i< ts.length; i++){
                tsBuf[i] = parseInt(ts[i],16);
            }
             timeStampBuff = Buffer.from(tsBuf,'hex');
        }else{
            let buf = new ArrayBuffer(1);
            let tsBuf = new Uint8Array(buf);
            tsBuf[0]=0x00;
            timeStampBuff = Buffer.from(tsBuf,'hex');
        }
        finalBuff.push(timeStampBuff);
    }
      if(taskextra.length >0){
          let buf = new ArrayBuffer(taskextra.length);
          let teBuf = new Uint8Array(buf);
          for(let i = 0; i<taskextra.length; i++){
              if(taskextra[i]>255){
                  throw new RangeError(`Extra info ${i+1} value limit exceeded.
                      Value ranges from 0 to 255` );
              }
              teBuf[i] = taskextra[i];
          }
          let extraBuf = Buffer.from(teBuf,'hex');
          finalBuff.push(extraBuf);
      }
      let mAB = abs.stringToArrayBuffer(taskmessage);
      let msgBuf = Buffer.from(mAB,'hex');
      finalBuff.push(msgBuf);
      let fin = Buffer.concat(finalBuff);
      // let cr = crc.crc8(fin).toString('hex');
      // fin = Buffer.concat([fin,Buffer.from(cr,'hex')]);
      return { 
          base64: fin.toString('base64'),
          hex:fin.toString('hex')
      }
}

function getTaskExtras(type){
  return 1
}

const decodeTask=(msgArray)=>{
  const taskToken = parseInt(msgArray[0],16);
  const taskClass = parseInt(msgArray[1],16);
  const taskType = taskClass >>2;
  const taskPriority = taskClass & 0b11;
  const isTaskOptional = parseInt(msgArray[2],16);
  const extras = getTaskExtras(taskType);
  const extraVals = msgArray.slice(3,3+extras);
  let curIndi=3+extras;
  const time_min = parseInt(msgArray[curIndi],16)*30;
  var timestamp=00;
  if(time_min== 0){
    if(msgArray[curIndi+1]!=0){
      console.log("in dec time")
      timestamp = msgArray.slice(curIndi+1, curIndi+5);
      timestamp = parseInt(timestamp.join("").toString(),16);
      curIndi = curIndi+5;
  }else{
      curIndi = curIndi +2;
  }
  }else{
    curIndi = curIndi+1;
  }
  var taskMessage = msgArray.slice(curIndi).join("");
  taskMessage = Buffer.from(taskMessage,'hex');
  taskMessage = abs.arraybufferToString(taskMessage);
  return({
      taskType:taskType,
      taskPriority:taskPriority,
      isTaskOptional:isTaskOptional,
      taskExtra:extraVals,
      time_min:time_min,
      timestamp : timestamp,
      message : taskMessage,
      token:taskToken
  });
}

exports.encodeTask = encodeTask;
exports.decodeTask = decodeTask;