const abs = require('./s2ab2s');
// const crc = require('node-crc');
const encodeMessage=({type,message,token})=>{
    if(typeof type === "undefined" || typeof message === "undefined" || typeof token === "undefined"){
        throw new TypeError("One or more arguments are undefined")
    }
    if(type>255) throw new RangeError("Type value limit exceeded. Value ranges from 0 to 255");
    if(token>255) throw new RangeError("Token value limit exceeded. Value ranges from 0 to 255");
    if(message.length > 45) throw new RangeError("Message length exceeded. Maximum is 45 characters");
    let buf = new ArrayBuffer(3);
    let msgProps = new Uint8Array(buf);
    msgProps[0] = 0x00;
    msgProps[1] = token;
    msgProps[2] = type;
    let propBuf = Buffer.from(msgProps,'hex');
    let mAB = abs.stringToArrayBuffer(message);
    let msgBuf = Buffer.from(mAB,'hex');
    let fin = Buffer.concat([propBuf, msgBuf]);
    // let cr = crc.crc8(fin).toString('hex');
    // fin = Buffer.concat([fin,Buffer.from(cr,'hex')]);
    return { 
        base64: fin.toString('base64'),
        hex:fin.toString('hex')
      }
}

const decodeMessage=(msgarray)=>{
    let messageToken = parseInt(msgarray[0],16);
    let messageType = parseInt(msgarray[1],16);
    var message = msgarray.slice(2).join("");
    message = Buffer.from(message,'hex');
    message = abs.arraybufferToString(message)
    return {
        messageType:messageType,
        message:message,
        token:messageToken
    };
}

exports.encodeMessage = encodeMessage;
exports.decodeMessage = decodeMessage;

// console.log(encodeMessage("HEMPLI"));
// console.log(decodeMessage('48454d504c49'));