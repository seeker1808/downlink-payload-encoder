const Task =  require('./src/task');
const Alarm = require('./src/alarm');
const Message = require('./src/message');

/////////////////////////  DECODE /////////////////////////
const messageTypes = {
    MESSAGE:'00',             //0
    ALARM:'01',               //1
    TASK:'11'                 //17
}

const decodePayload=(taskstring)=>{
    const msgBytes = Buffer.from(taskstring,'base64').toString('hex');
    const crcVal = msgBytes.slice(-2);
    const hexString = msgBytes.slice(0,-2);
    // const calcCrcVal = crc.crc8(Buffer.from(hexString,'hex')).toString('hex');
    // if(crcVal.toString() != calcCrcVal.toString()) return ("Invalid Paylod. CRC Failed")
    const hexArray = hexString.match(/.{1,2}/g);
    const msgType = hexArray[0];
    const payload = hexArray.slice(1)
    let decodedMsg = null;
    let payloadType = null;
    switch(msgType){
        case messageTypes.MESSAGE:
            decodedMsg = Message.decodeMessage(payload);
            payloadType = 'message';
        break;
        case messageTypes.ALARM:
            decodedMsg = Alarm.decodeAlarm(payload);
            payloadType = 'alarm';
        break;
        case messageTypes.TASK:
            decodedMsg = Task.decodeTask(payload);
            payloadType = 'task';
        break;
        default:
            console.log("Unknow Message");
        break;
    }
    return {
        type : payloadType,
        payload: decodedMsg
    };
  }
///////////////////////////////////////////////////////////////////////
module.exports = {
    Task,
    Alarm,
    Message,
    decode:decodePayload
};